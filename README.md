# README
TODO PLAN
# hardware test bed
- [] Build test tubes
- [] optocoupler motor mount
- [] light input mount
  - finalize light tube OD
  - order orings for light tube size


# features working:
- [x] DS18B20
- [x] H710B (ish?) with bridge offset
- [x] relay
- [] air pump
- [] Optical Density measuremnt
- [] drive motor h bridge, half the quad for motor, half for the 2 peristatlic pumps
- [] drive motor RPM
- [] heater PID
- [] light dimmer module
- [] SPIFFS storage

# Data Layers
- [] Data export api
- [] Serial data export format
