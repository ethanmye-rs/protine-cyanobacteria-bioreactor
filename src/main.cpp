#include <Arduino.h>

// import for internal ESP storage
#include "SPIFFS.h"

// Import for webserver
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Arduino_JSON.h>

// Import for SD card via SPI
#include "FS.h"
#include "SD.h"
#include "SPI.h"

// Pressure Sensor lib
#include <Q2HX711.h>

// heater loop
#include <PID_v1.h> //https://github.com/br3ttb/Arduino-PID-Library/blob/master/examples/PID_AdaptiveTunings/PID_AdaptiveTunings.ino

// Onewire Stuff
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 4

#define HEAT_OUTPUT_PIN 7
#define HEAT_TEMP_PIN 4

// 34 FSPICS0    CS //default CS
// 35 FSPID      DI (MOSI)
// 36 FSPICLK    CLK
// 37 FSPIQ      DOUT (MISO)

// #double heat_setpoint, heat_input, heat_output;
//  PID heat_loop(&heat_input, &heat_output, &heat_setpoint, 2, 5, 1, P_ON_M, DIRECT);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature temp_sensor(&oneWire);

const byte MPS_OUT_pin = 16; // OUT data pin
const byte MPS_SCK_pin = 17; // clock data pin
int avg_size = 10;           // #pts to average over

Q2HX711 MPS20N0040D(MPS_OUT_pin, MPS_SCK_pin); // start comm with the HX710B

void TEST_spiffs()
{
  File file = SPIFFS.open("/test.txt");
  if (!file)
  {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.println("File Content:");
  while (file.available())
  {
    Serial.write(file.read());
  }
  file.close();
}

void setup()
{
  // Serial Block
  Serial.begin(115200);

  /*
    if (!SPIFFS.begin())
    {
      Serial.println("Internal SPIFFS Failed");
      return;
    }

    if (!SD.begin())
    {
      Serial.println("Card Mount Failed");
      return;
    }

    uint8_t cardType = SD.cardType();
    if (cardType == CARD_NONE)
    {
      Serial.println("No SD card attached");
      return;
    }
  */

  temp_sensor.begin(); // don't forget 10k resistor between data and vcc
}

void loop()
{
  // avg_val *= 0.00347;
  // avg_val -= 56506;
  //Serial.println(MPS20N0040D.read() / 100.0); // print out the average

  temp_sensor.requestTemperatures();
  float temperatureC = temp_sensor.getTempCByIndex(0);
  Serial.print(temperatureC);
  Serial.println("C");
  delay(1000);
}